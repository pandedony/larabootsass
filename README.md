<h2 align="center">Bootstrap npm starter template for Laravel with Sass</h2>
<p align="center"> <strong> V 1 <strong> </p>

<p align="center">Create new Bootstrap-powered laravel projects with sass in no time.</p>

## About

- This project is for people who want to learn scss and bootstrap in a simple way.
- You can also customize bootstrap according to what you need for your website project.

## What's included

- Single HTML page (`resources/views/welcome.blade.php`) to demonstrate how to include Bootstrap.
- Includes [Bootstrap](https://getbootstrap.com) (currently using v5.2.3) source files via npm.
- npm scripts (see `package.json`) for compiling and autoprefixing Sass, watching for changes.
- (`webpack.mix.js`) for run laravel-mix.
- Example stylesheet (`resources/scss/mix.scss`) highlighting two ways to include and customize Bootstrap.
- Example JavaScript folder (`resources/custom`) to put your custom js.

## Important
- Please don't modify (`resources/js/app.js`)
- Please check the file (`resources/scss/mix.scss`) to make sure what bootstrap scripts you need in your project by uncommenting some of the scripts, or you can visit 
[Bootstrap](https://getbootstrap.com/docs/5.0/customize/sass/) for the doumentation.
- Be sure to have [Node.js](https://nodejs.org/) installed before proceeding. **We recommend using Node's LTS releases, which is currently at v16.x. We only test our compiled CSS against v16.**
</br>

- <b>For single compile</b>
```shell
npm run dev
```
- <b>For compile and watching</b>
```shell
npm run watch
```
- <b>For minify</b>
```shell
npm run prod
```


## Usage
<h3>There are two ways to run the application</h3>

1. Clone repository
2. Follow the intruction manually

## Clone repository

- Clone laravelsass from repo
```shell
git clone https://gitlab.com/pandedony/larabootsass
```
- Go to folder larabootsass
- Run laravel composer
```shell
composer install
```
```shell
php artisan key:generate
```
- Install dependencies
```shell
npm install
```
- run laravel-mix
```shell
npm run watch
```

## Manuals

1. Update package.json (`package.json`) in root folder from this :
```shell
{
    "private": true,
    "scripts": {
        "dev": "vite",
        "build": "vite build"
    },
    "devDependencies": {
        "axios": "^1.1.2",
        "laravel-vite-plugin": "^0.7.2",
        "lodash": "^4.17.19",
        "postcss": "^8.1.14",
        "vite": "^4.0.0"
    }
}
```
To this :
```shell
{
    "private": true,
    "scripts": {
        "dev": "npm run development",
        "development": "mix",
        "watch": "mix watch", 
        "watch-poll": "mix watch -- --watch-options-poll=1000",
        "hot": "mix watch --hot",
        "prod": "npm run production",
        "production": "mix --production"
    },
    "devDependencies": {
        "axios": "^1.1.2",
        "bootstrap": "^5.2.3",
        "laravel-mix": "^6.0.49",
        "laravel-vite-plugin": "^0.7.2",
        "lodash": "^4.17.19",
        "postcss": "^8.1.14",
        "resolve-url-loader": "^5.0.0",
        "sass": "^1.57.1",
        "sass-loader": "^13.2.0",
        "vite": "^4.0.0"
    }
}

```
2. run npm install to install dependencies
```shell
  npm install
```
3. Inside folder resources(`resources`), create folder named : custom (`resources/custom`), and place your custom js here

4. Inside folder resources(`resources/css`),  place your custom css here

5. Inside folder (`resources`), create folder named : scss (`resources/scss`). inside folder scss create file named mix.scss (`resources/scss/mix.scss`) and paste the script below
```shell
// Override Bootstrap's Sass default variables
//
// Nearly all variables in Bootstrap are written with the `!default` flag.
// This allows you to override the default values of those variables before
// you import Bootstrap's source Sass files.
//
// Overriding the default variable values is the best way to customize your
// CSS without writing _new_ styles. For example, change you can either change
// `$body-color` or write more CSS that override's Bootstrap's CSS like so:
// `body { color: red; }`.


//
// Bring in Bootstrap
//

// Option 1
//
// Import all of Bootstrap's CSS

// @import "./node_modules/bootstrap/scss/bootstrap";

// Option 2
//
// Place variable overrides first, then import just the styles you need. Note that some stylesheets are required no matter what.


// Customize some defaults
// Example
$primary: #000;
$success: #7952b3;


 
// This only for import all of bootstrap's CSS
// Note : This not recomended to use, please use what you need only
// @import "./node_modules/bootstrap/scss/bootstrap"; 

// Required
@import "./node_modules/bootstrap/scss/functions";
@import "./node_modules/bootstrap/scss/variables";
@import "./node_modules/bootstrap/scss/maps";
@import "./node_modules/bootstrap/scss/mixins";
@import "./node_modules/bootstrap/scss/utilities";
@import "./node_modules/bootstrap/scss/root";
@import "./node_modules/bootstrap/scss/reboot";


// Please go bootstrap.com to know what your website need
@import "./node_modules/bootstrap/scss/type";
// @import "./node_modules/bootstrap/scss/images";
@import "./node_modules/bootstrap/scss/containers";
@import "./node_modules/bootstrap/scss/grid";
// @import "./node_modules/bootstrap/scss/tables";
// @import "./node_modules/bootstrap/scss/forms";
@import "./node_modules/bootstrap/scss/buttons";
@import "./node_modules/bootstrap/scss/transitions";
@import "./node_modules/bootstrap/scss/dropdown";
// @import "./node_modules/bootstrap/scss/button-group";
@import "./node_modules/bootstrap/scss/nav";
@import "./node_modules/bootstrap/scss/navbar"; // Requires nav
@import "./node_modules/bootstrap/scss/card";
// @import "./node_modules/bootstrap/scss/breadcrumb";
// @import "./node_modules/bootstrap/scss/accordion";
// @import "./node_modules/bootstrap/scss/pagination";
// @import "./node_modules/bootstrap/scss/badge";
// @import "./node_modules/bootstrap/scss/alert";
// @import "./node_modules/bootstrap/scss/progress";
// @import "./node_modules/bootstrap/scss/list-group";
@import "./node_modules/bootstrap/scss/close";
// @import "./node_modules/bootstrap/scss/toasts";
@import "./node_modules/bootstrap/scss/modal"; // Requires transitions
// @import "./node_modules/bootstrap/scss/tooltip";
@import "./node_modules/bootstrap/scss/popover";
@import "./node_modules/bootstrap/scss/carousel";
// @import "./node_modules/bootstrap/scss/spinners";
@import "./node_modules/bootstrap/scss/offcanvas"; // Requires transitions
// @import "./node_modules/bootstrap/scss/placeholders";


```


6. create file in your root folder : webpack.mix.js (`webpack.mix.js`)

7. paste code below to webpack.mix.js
```shell
const mix = require('laravel-mix');
require('laravel-mix-copy-watched')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
.copy('resources/custom/**/*.js', 'public/dist/custom')
.copy('resources/css/**/*.css', 'public/dist/css')
.js('resources/js/app.js', 'public/dist/js')
.sass('resources/scss/mix.scss', 'public/dist/css');
```


8. Run laravel mix
```shell
  npm run watch
```

9. Call Mix Css and Js file inside blade
- Css
```shell
  <link rel="stylesheet" href="{{asset(('dist/css/mix.css'))}}">
```
- Js
```shell
  <script src="{{asset(('dist/js/app.js'))}}"></script>
```

## Copyright

&copy; Pande Dony 2022.
