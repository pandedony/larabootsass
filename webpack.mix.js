const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
.copy('resources/custom/**/*.js', 'public/dist/custom')
.copy('resources/css/**/*.css', 'public/dist/css')
.js('resources/js/app.js', 'public/dist/js')
.sass('resources/scss/mix.scss', 'public/dist/css');